# NFS Client infra

## Install harbor with helm chart

```bash
helm repo add ckotzbauer https://ckotzbauer.github.io/helm-charts
kubectl create ns harbor
helm upgrade  nfs-client-infra ckotzbauer/nfs-client-provisioner   --namespace kube-system  -f nfs-client-values.yaml --install
```
